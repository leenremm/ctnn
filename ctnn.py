#!/usr/bin/python

import sys

import os
os.environ["KERAS_BACKEND"] = "theano"

from keras.layers import Input, Dense
from keras.models import Sequential, Model
from keras.datasets import mnist
from keras.models import model_from_json, load_model
from keras.optimizers import Adam

import matplotlib.pyplot as plt
from datetime import datetime
import numpy as np

from numpy.random import seed
seed(1)

# =====================================================================================

def init():
    pass

# =====================================================================================

# Custom LOSS functions for Keras (https://stackoverflow.com/questions/48744945/keras-ms-ssim-as-loss-function)

def keras_SSIM_cs(y_true, y_pred):
    axis=None
    gaussian = make_kernel(1.5)
    x = tf.nn.conv2d(y_true, gaussian, strides=[1, 1, 1, 1], padding='SAME')
    y = tf.nn.conv2d(y_pred, gaussian, strides=[1, 1, 1, 1], padding='SAME')

    u_x=K.mean(x, axis=axis)
    u_y=K.mean(y, axis=axis)

    var_x=K.var(x, axis=axis)
    var_y=K.var(y, axis=axis)

    cov_xy=cov_keras(x, y, axis)

    K1=0.01
    K2=0.03
    L=1  # depth of image (255 in case the image has a differnt scale)

    C1=(K1*L)**2
    C2=(K2*L)**2
    C3=C2/2

    l = ((2*u_x*u_y)+C1) / (K.pow(u_x,2) + K.pow(u_x,2) + C1)
    c = ((2*K.sqrt(var_x)*K.sqrt(var_y))+C2) / (var_x + var_y + C2)
    s = (cov_xy+C3) / (K.sqrt(var_x)*K.sqrt(var_y) + C3)

    return [c,s,l]

def keras_MS_SSIM(y_true, y_pred):
    iterations = 5
    x=y_true
    y=y_pred
    weight = [0.0448, 0.2856, 0.3001, 0.2363, 0.1333]
    c=[]
    s=[]
    for i in range(iterations):
        cs=keras_SSIM_cs(x, y)
        c.append(cs[0])
        s.append(cs[1])
        l=cs[2]
        if(i!=4):
            x=tf.image.resize_images(x, (x.get_shape().as_list()[1]//(2**(i+1)), x.get_shape().as_list()[2]//(2**(i+1))))
            y=tf.image.resize_images(y, (y.get_shape().as_list()[1]//(2**(i+1)), y.get_shape().as_list()[2]//(2**(i+1))))
    c = tf.stack(c)
    s = tf.stack(s)
    cs = c*s

    l=(l+1)/2
    cs=(cs+1)/2

    cs=cs**weight
    cs = tf.reduce_prod(cs)
    l=l**weight[-1]

    ms_ssim = l*cs
    ms_ssim = tf.where(tf.is_nan(ms_ssim), K.zeros_like(ms_ssim), ms_ssim)

    return K.mean(ms_ssim)

# =====================================================================================

def init_nn(input_dim=784, encoding_dim=32):

    # https://blog.keras.io/building-autoencoders-in-keras.html
    # https://towardsdatascience.com/auto-encoder-what-is-it-and-what-is-it-used-for-part-1-3e5c6f017726
    # https://medium.com/datadriveninvestor/simple-autoencoders-using-keras-6e67677f5679

    autoencoder = Sequential()
    autoencoder.add(Dense(512, activation='elu', input_shape=(input_dim,)))
    autoencoder.add(Dense(encoding_dim, activation='elu', name="encoded"))
    autoencoder.add(Dense(512, activation='elu'))
    autoencoder.add(Dense(input_dim, activation='sigmoid'))
    autoencoder.compile(loss='mean_squared_error', optimizer = Adam(), metrics=['accuracy'])
    #autoencoder.compile(loss='binary_crossentropy', optimizer = Adam(), metrics=['accuracy'])

    # Loss Options:
    #mean_squared_error - The mean squared error is popular for function approximation (regression) problems
    #mean_absolute_error
    #mean_absolute_percentage_error
    #mean_squared_logarithmic_error
    #squared_hinge
    #hinge
    #categorical_hinge
    #logcosh
    #categorical_crossentropy
    #sparse_categorical_crossentropy - The cross-entropy error function is often used for classification problems when outputs are interpreted as probabilities of membership in an indicated class
    #binary_crossentropy
    #kullback_leibler_divergence
    #poisson
    #cosine_proximity

    # Encoder Model
    encoder = Model(autoencoder.input, autoencoder.get_layer('encoded').output)

    # Decoder Model
    encoded_input = Input(shape=(encoding_dim,))
    decoder = autoencoder.layers[-2](encoded_input)
    decoder = autoencoder.layers[-1](decoder)
    decoder = Model(encoded_input, decoder)

    return autoencoder, encoder, decoder

# =====================================================================================

def load_data():

    (x_train, y_train), (x_test, y_test) = mnist.load_data()

    # Normalize all values between 0 and 1
    x_train = x_train.astype('float32') / 255.
    x_test = x_test.astype('float32') / 255.

    # Flatten the 28x28 images into vectors of size 784
    x_train = x_train.reshape((len(x_train), np.prod(x_train.shape[1:])))
    x_test = x_test.reshape((len(x_test), np.prod(x_test.shape[1:])))

    return x_train, x_test

# =====================================================================================

def save_to_file(model, filename="model", path="saved_models//"):

    # Save weights
    model.save_weights("%s%s.h5" % (path, filename))

    # Save model architecture
    #with open("%s%s.json" % (path, filename), 'w') as f:
    #    f.write(model.to_json())

# =====================================================================================

def load_from_file(model, filename="model", path="saved_models//"):

    # Load model architecture
    #with open("%s%s.json" % (path, filename), 'r') as f:
    #    model = model_from_json(f.read())

    # Load model weights
    model.load_weights("%s%s.h5" % (path, filename))

# =====================================================================================

def train_nn(autoencoder, x_train, x_test, epochs_n=50):

    history = autoencoder.fit(x_train, x_train, epochs=epochs_n, batch_size=256, shuffle=True, validation_data=(x_test, x_test))

    plt.plot(history.history['loss'])
    plt.plot(history.history['val_loss'])
    plt.title('model accuracy')
    plt.ylabel('loss')
    plt.xlabel('epoch')
    plt.legend(['training data loss', 'testing data loss'], loc='upper left')
    plt.show()

    return autoencoder, history

# =====================================================================================

def reconstruct(encoder, decoder, x_test):

    encoded_imgs = encoder.predict(x_test)
    decoded_imgs = decoder.predict(encoded_imgs)

    return encoded_imgs, decoded_imgs

# =====================================================================================

def show_images(img_set_1, img_set_2, n=10):

    plt.figure(figsize=(10, 4))
    for i in range(n):

        # Display original
        ax = plt.subplot(2, n, i + 1)
        plt.imshow(img_set_1[i].reshape(int(len(img_set_1[i])/28), 28))
        ax.get_xaxis().set_visible(False)
        ax.get_yaxis().set_visible(False)

        # Display reconstruction
        ax = plt.subplot(2, n, i + 1 + n)
        plt.imshow(img_set_2[i].reshape(int(len(img_set_2[i])/28), 28))
        ax.get_xaxis().set_visible(False)
        ax.get_yaxis().set_visible(False)

    plt.show()

# =====================================================================================

def show_duration(startTime):

    if startTime == 0:
        return datetime.now()

    total_time = datetime.now() - startTime
    print ("  >> Complete. Script Duration: %s\n" % (total_time))
    return datetime.now()

# =====================================================================================

init()

# =====================================================================================
