import os
os.environ["KERAS_BACKEND"] = "theano"
import pickle
from keras.datasets import mnist
from keras.utils import to_categorical
import random
import numpy as np
import matplotlib.pyplot as plt
import glob
import cv2
random.seed(0)

# ==========================================

def load_nn_from_file(filename="model.h5", path="saved_nn_models"):

    # ===========================================================================
    # Load Method
    # ===========================================================================

    save_dir = os.path.join(os.getcwd(), path)
    model_path = os.path.join(save_dir, filename)
    model = load_model(model_path)
    #print('Model Loaded: %s ' % model_path)

    return model

# =====================================================================================

def grayscale(img_original):

    img = img_original.copy()
    img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    return img

# =====================================================================================

def open_image(filename):

    # Opens the file
    if not(os.path.isfile(filename)):
        return False, None # Exception if file does not exist
    try:
        img = cv2.imread(filename)
    except:
        return False, None # Exception if file cannot open
    return True, img

# =====================================================================================

def load_nn_data(training_dir, testing_dir):

    # Training images
    train_images = []
    train_labels = []
    filename_array = glob.glob(training_dir)
    for filename in filename_array:
        status, new_file = open_image(filename)
        img = grayscale(new_file)
        train_images.append(img)
        train_labels.append(int(filename[-39:-37]))
    train_images = np.array(train_images)
    train_labels = np.array(train_labels)

    # Testing images
    test_images = []
    test_labels = []
    filename_array = glob.glob(testing_dir)
    for filename in filename_array:
        status, new_file = open_image(filename)
        img = grayscale(new_file)
        test_images.append(img)
        test_labels.append(int(filename[-39:-37]))
    test_images = np.array(test_images)
    test_labels = np.array(test_labels)

    # Normalize all values between 0 and 1
    train_images = train_images.astype('float32') / 255.
    test_images = test_images.astype('float32') / 255.

    # Resize to 28 x 28
    train_images = np.array([1 - cv2.resize(image, (28,28)) for image in train_images])
    test_images = np.array([1 - cv2.resize(image, (28,28)) for image in test_images])

    # Flatten the 28x28 images into vectors of size 784
    train_images = train_images.reshape((-1, 28, 28, 1))
    test_images = test_images.reshape((-1, 28, 28, 1))

    return train_images, train_labels, test_images, test_labels

# =====================================================================================

def load_raw_data():

    # One tone for each number
    audio_list_10 = to_categorical(range(10))
    audio_list_28 = cv2.resize(audio_list_10, (28,10))
    audio_dictionary = {}
    for i in range(len(audio_list_28)):
        audio_dictionary[str(i)] = [np.array(audio_list_28[i])]

    # Visual training & testing
    #(x_train, y_train), (x_test, y_test) = mnist.load_data()
    x_train, y_train, x_test, y_test = load_nn_data('dataset\\courier\\*.png', 'dataset\\courier\\*.png')

    # Visual dictionary
    visual_dictionary = {}
    i = 0
    while i < len(y_train):
        label = str(y_train[i])
        value = x_train[i].reshape(28 * 28,)
        if label not in visual_dictionary:
            visual_dictionary[label] = []
        visual_dictionary[label].append(value)
        i += 1

    return audio_dictionary, visual_dictionary, x_train, y_train, x_test, y_test

# ==========================================

def get_sample(dictionary, key):
    return random.choice(dictionary[key])

# ==========================================

def show_images(img_set):

    plt.figure(figsize=(10, 4))

    n = len(img_set[0])

    for i in range(n):

        for j in range(len(img_set)):

            ax = plt.subplot(6, n, i + 1 + n*j)
            plt.imshow(img_set[j][i].reshape(int(len(img_set[j][i])/28), 28))
            ax.get_xaxis().set_visible(False)
            ax.get_yaxis().set_visible(False)

    plt.show()

# ==========================================

def calculate_duplicates(array):
    i = 0
    changes = 0
    max_changes = len(array)-1
    while i < max_changes:
        if array[i] != array[i+1]:
            changes += 1
        i += 1
    return changes, max_changes, round(changes/max_changes,5)

# ==========================================

def generate_sequence(sequence_length=10, target_change_percentage=1):

    if (target_change_percentage > 1):
        target_change_percentage = 1
    elif (target_change_percentage < 0):
        target_change_percentage = 0

    # Create sequence with no duplicates
    sequence = []
    for i in range(sequence_length):
        sequence.append(i % 9)

    # Create duplicates
    changes, max_changes, changes_percent = calculate_duplicates(sequence)
    i = 0
    while (True):
        if (changes_percent <= target_change_percentage) or (i > max_changes):
            break
        sequence[i] = 9
        changes, max_changes, changes_percent = calculate_duplicates(sequence)
        i += 1

    return sequence, changes_percent

# ==========================================

def load_data(sequence_length=10, target_change_percentage=1, audio_noise=0, image_noise=0):

    a, v, x_train, y_train, x_test, y_test = load_raw_data()

    # ==========================================

    sequence, changes_percent = generate_sequence(sequence_length, target_change_percentage)

    # ==========================================
    # TRAINING
    # ==========================================

    av_x_train = []

    for i in range(len(x_train)):
        v_sample = x_train[i].reshape((784,))
        v_sample /= max(v_sample)
        a_sample = get_sample(a, str(y_train[i]))
        a_sample = a_sample / max(a_sample)
        a_sample = [[i]*28 for i in a_sample]
        a_sample = [ item for innerlist in a_sample for item in innerlist ]
        a_sample = np.array(a_sample)
        av_x_train.append(np.concatenate((v_sample, a_sample)))

    # Datasets created: av_x_train, y_train

    # ==========================================
    # TESTING
    # ==========================================

    audio_samples = []
    image_samples = []
    combined_value = []
    combined_label = []

    for i in range(len(sequence)):

        # Audio
        audio = get_sample(a, str(sequence[i]))
        audio = audio / max(audio)
        audio = [[i]*28 for i in audio]
        audio = [ item for innerlist in audio for item in innerlist ]
        audio = np.array(audio)
        audio = add_noise(audio, audio_noise)    # Add noise
        audio_samples.append(audio)

        # Image
        image = get_sample(v, str(sequence[i]))
        image = image / max(image)
        image = add_noise(image, image_noise)    # Add noise
        image_samples.append(image)

        # Combined
        combined_value.append(np.concatenate((image, audio)))
        combined_label.append(sequence[i])

    #show_images(image_samples, audio_samples, combined_value)

    # Datasets created:
    # av_x_train        - training (audio and video)        - len: 60000 x 1568
    # y_train           - training labels                   - len: 60000 x 1
    # combined_value    - testing (audio and video)         - len: defined in init
    # sequence          - testing labels                    - len: defined in init

    return np.array(av_x_train), np.array(y_train), np.array(combined_value), np.array(combined_label)

# ==========================================

def add_noise(image, amount=0.5, orientation=0):

    dim = image.size
    output = image.copy()

    if (orientation == 0):
        start = 0
        end = int(dim*amount)
    else:
        start = int(dim*amount)
        end = 0

    output[start:end] = 0

    return output

# ==========================================
