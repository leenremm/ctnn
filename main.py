#!/usr/bin/python

import ctnn
import mnist_av
import numpy as np
import random

# =====================================================================================
# User defined parameters
# =====================================================================================

nn_method = "load" # "train" = train from data, or "load" = load from saved file
epochs = 500
sequence_length = 100

# =====================================================================================
# Time Script
# =====================================================================================

startTime = ctnn.show_duration(0)

# =====================================================================================
# Step 010: Load MNIST data
# =====================================================================================

print ("\n=====================================")
print ("PART 1: Initializing Model")
print ("=====================================\n")

input("Press Enter to continue...")

print ("\nLoading Courier Typeset data...")
#x_train, x_test = ctnn.load_data()             # Visual Only
x_train, y_train, x_test, y_test = mnist_av.load_data()    # Audio and Visual
#mnist_av.show_images([x_train[0:10],x_test[0:10]])
lastTime = ctnn.show_duration(startTime)

# =====================================================================================
# Step 020: Initialize NN
# =====================================================================================

print ("Initializing Autoencoder (Keras)...")
input_dim = len(x_train[0])
autoencoder, encoder, decoder = ctnn.init_nn(input_dim, 100)
lastTime = ctnn.show_duration(lastTime)

# =====================================================================================
# Step 030: Train Autoencoder
# =====================================================================================

if (nn_method == "train"):

    print ("Training Autoencoder...\n")
    autoencoder, history = ctnn.train_nn(autoencoder, x_train, x_test, epochs_n=epochs)

if (nn_method == "load"):

    print ("Loading Autoencoder training from file...")
    ctnn.load_from_file(autoencoder, "autoencoder")
    ctnn.load_from_file(encoder, "encoder")
    ctnn.load_from_file(decoder, "decoder")

lastTime = ctnn.show_duration(lastTime)

# =====================================================================================
# Step 045: Summary of models
# =====================================================================================

'''
print ("Summary of Autoencoder:")
autoencoder.summary()

print ("Summary of Encoder:")
encoder.summary()

print ("Summary of Decoder:")
decoder.summary()
'''

# =====================================================================================
# Step 045: PLotting Training
# =====================================================================================

lastTime = ctnn.show_duration(lastTime)

# =====================================================================================
# Step 060: Save to File
# =====================================================================================

print ("Saving to File...")
ctnn.save_to_file(autoencoder, "autoencoder")
ctnn.save_to_file(encoder, "encoder")
ctnn.save_to_file(decoder, "decoder")
lastTime = ctnn.show_duration(lastTime)

# =====================================================================================
# Step 040: Reconstruct Images
# =====================================================================================

print ("Reconstructing Images...")
encoded_imgs, decoded_imgs = ctnn.reconstruct(encoder, decoder, x_test)
lastTime = ctnn.show_duration(lastTime)

# =====================================================================================
# Step 050: Show Images
# =====================================================================================

print ("Showing Images...")
ctnn.show_images(x_test, decoded_imgs)
lastTime = ctnn.show_duration(lastTime)

# =====================================================================================
# Step 050: Show Images
# =====================================================================================

print ("\n=====================================")
print ("PART 2: Running Sequences")
print ("=====================================\n")

input("Press Enter to continue...")
print("")

random.seed(0)
a, v, _, _, _, _ = mnist_av.load_raw_data()

for diff_threshold in np.linspace(10, 50, 5):

    for i in np.linspace(1,0,101):

        # Create Sequence
        audio_noise=0.0
        image_noise=0.0

        _, _, x_test, y_test = mnist_av.load_data(sequence_length, i, audio_noise, image_noise)    # Audio and Visual

        # First Image (Blank)
        last_reconstruction = np.zeros(len(x_test[0]))

        # Threshold
        over_threshold = 0

        # Image Showing
        image_display = [[],[],[],[]]

        # Loop through each element in the sequence
        for j in range(len(x_test)):

            # Input image
            input_image = x_test[j]

            # Calculate difference (image)
            diff = (input_image - last_reconstruction)

            # Calculate difference (sum)
            diff_sum = sum(diff*diff)

            #print (round(i,2), j, y_test[j], diff_sum)

            # Threshold
            if (diff_sum > diff_threshold):
                # Increment counter
                over_threshold += 1
                # Update new_reconstruction
                new_reconstruction = ctnn.reconstruct(encoder, decoder, np.array([input_image]))[1][0]
            else:
                # Update last_reconstruction
                new_reconstruction = last_reconstruction
                pass

            # Display images
            image_display[0].append(last_reconstruction)
            image_display[1].append(input_image)
            image_display[2].append(diff)
            image_display[3].append(new_reconstruction)

            # Update last_reconstruction
            last_reconstruction = new_reconstruction

        # Print Results
        print ("Threshold, %3d, Rep, %3d, Sent, %3d, Total, %3d" % (diff_threshold, round((1-i)*100), over_threshold, len(decoded_imgs)))
        #print ("  >> Repetitions in sequence: %3d %%, Information sent to NN from difference engine %d/%d times." % (round((1-i)*10)*10, over_threshold, len(decoded_imgs)))

        #mnist_av.show_images(image_display)

print("")

lastTime = ctnn.show_duration(lastTime)

# =====================================================================================

print ("=====================================")
print ("PART 3: Running Occlusion Test")
print ("=====================================\n")

input("Press Enter to continue...")
print("")

random.seed(0)
a, v, _, _, _, _ = mnist_av.load_raw_data()

# Vary audio occlusion percentage
for audio_noise in np.linspace(1,0,5):

    # Vary visual occlusion percentage
    for image_noise in np.linspace(1,0,5):

        sum_differences = 0

        # Vary duplicates in the sequence
        sequence_permutations = 10
        for i in np.linspace(1,0,sequence_permutations):

            _, _, x_test_original, _ = mnist_av.load_data(sequence_length, i, 0, 0)    # Audio and Visual
            _, _, x_test, y_test = mnist_av.load_data(sequence_length, i, audio_noise, image_noise)    # Audio and Visual

            # Encode and Decode x_test
            encoded_imgs, decoded_imgs = ctnn.reconstruct(encoder, decoder, x_test)

            # Encode and Decode x_test
            encoded_imgs_original, decoded_imgs_original = ctnn.reconstruct(encoder, decoder, x_test_original)

            # First Image (Blank)
            last_reconstruction = np.zeros(len(x_test[0]))

            image_display = [[],[],[],[],[],[]]

            # Loop through each element in the sequence
            for j in range(len(decoded_imgs)):

                # Input image
                input_image_original = x_test_original[j]
                input_image = x_test[j]

                # Current reconstruction
                current_reconstruction = decoded_imgs[j]    # Values 0-1
                current_reconstruction_original = decoded_imgs_original[j]    # Values 0-1

                # Calculate difference (image)
                diff = (current_reconstruction - current_reconstruction_original)

                # Calculate difference (sum)
                diff_sum = sum(diff*diff)

                sum_differences += diff_sum

                image_display[0].append(last_reconstruction)
                image_display[1].append(input_image_original)
                image_display[2].append(input_image)
                image_display[3].append(current_reconstruction)
                image_display[4].append(current_reconstruction_original)
                image_display[5].append(diff)

                # Update last_reconstruction
                last_reconstruction = current_reconstruction

        if (image_noise == 0.0) and (audio_noise == 1.0):
            #mnist_av.show_images(image_display)
            pass

        if (image_noise == 1.0) and (audio_noise == 0.0):
            #mnist_av.show_images(image_display)
            pass

        print ("Audio occlusion: %3d%%, Visual occlusion: %3d %%, MSE score: %9s" % (int(100*round(audio_noise,2)), int(100*round(image_noise,2)), '{:,}'.format(int(sum_differences))))

print("")

lastTime = ctnn.show_duration(lastTime)

# =====================================================================================
# Step 099: Script Time
# =====================================================================================

print ("=====================================")
print ("End of Script")
print ("=====================================\n")

endTime = ctnn.show_duration(startTime)
